Manual page for irsd\_sa\_unwrap
================================

SYNOPSIS
--------

irsd\_sa\_unwrap [**--from** *<FROM\_FILE>*] [**--to** *<TO\_FILE>*]

irsd\_sa\_unwrap (**-h** \| **--help**)

irsd\_sa\_unwrap **--usage**

irsd\_sa\_unwrap.py **--version**


DESCRIPTION
-----------

irsd\_sa\_unwrap unwrap a mail bundled by *SpamAssassin*.

it parses a *rfc2822* email message and unwrap it if contains spam
attached.


OPTIONS
-------

**-h**, **--help**
    Show the help screen
**--usage**
    Show usage information
**--version**
    Show version information

**-f** *FILE*, **--from**\ =\ *FILE*
    Filename of the email to read and unwrap. If not informed, the stdin
    will be used
**-t** *FILE*, **--to**\ =\ *FILE*
    Filename to write the unwrapped email. If not informed, the stdout
    will be used

SEE ALSO
--------

`spamassassin(1)`,
`Mail::SpamAssassin::Conf(3)`.

The full documentation for irsd is maintained in https://irsd.readthedocs.io/

BUGS
----

You can report bugs on https://github.com/irsd/irsd/issues
