API Reference
=============

.. toctree::
   :maxdepth: 3
   :glob:
   :includehidden:

   modules

Main Objects and functions
--------------------------
.. autosummary::
    :toctree:  main functions and object

   irsd.learn_mail
   irsd.test_mail
   irsd.unwrap
   irsd.irsd.__version__
   irsd.irsd.__exitcodes__
   irsd.irsd.irsd
   irsd.irsd.irsdError


Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
